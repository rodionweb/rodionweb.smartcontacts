<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
$this->setFrameMode(true);
?>

<?if(count($arResult["PHONES"]) > 0):?>
    <div>
        <?foreach($arResult["PHONES"] as $arPhone):?>
            <div><a href="tel:<?=$arPhone["HREF"]?>"><?=$arPhone["TEXT"]?></a></div>
        <?endforeach?>
    </div>
<?endif?>