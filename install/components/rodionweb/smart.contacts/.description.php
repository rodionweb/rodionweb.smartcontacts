<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SMART_CONTACTS"),
	"DESCRIPTION" => GetMessage("SMART_CONTACTS_DESCRIPTION"),
	"ICON" => "/images/contacts.gif",
	"CACHE_PATH" => "Y",
	"SORT" => 70,
	"PATH" => array(
		"ID" => "rodionweb",
		"CHILD" => array(
			"ID" => "rodionweb_smartcontacts",
			"NAME" => GetMessage("CONTACTS"),
			"SORT" => 30,
		),
	),
);
?>