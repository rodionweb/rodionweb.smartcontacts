<?php
/**
 * User: Rodion Abdurakhimov
 * Mail: rodion.arr.web@gmail.com
 * Date: 1/2/15
 * Time: 12:22
 */

$MESS["CONTACTS_MODULE_NAME"] = "SmartContacts";
$MESS["CONTACTS_MODULE_DESCRIPTION"] = "После установки вы сможете легко управлять телефонами вашего магазина в шапке";
